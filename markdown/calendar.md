| Semaine | Date                     | Sujet      | Détail | Evaluation             |
|---------|--------------------------|------------|--------|------------------------|
| 1       | 20.2-24.2                | Chapitre 1 |        |                        |
| 2       | 27.2-3.3                 |            |        | Contrôle des classeurs |
| 3       | 6.3-10.3                 |            |        |                        |
| 4       | 13.3-17.3                | Chapiter 2 |        | Contrôle des classeurs |
| 5       | 20.3-24.3                |            |        |                        |
| 6       | 27.3-31.3                |            |        | Contrôle des classeurs |
|         | Vacances de Pâques       |            |        |                        |
| 7       | 17.4-21.4                |            |        | Contrôle des classeurs |
| 8       | 24.4-28.4                |            |        | DEC II,1               |
| 9       | 1.5-5.5 (-2)             |            |        | Contrôle des classeurs |
| 10      | 8.5-12.5                 | Chapitre 3 |        |                        |
| 11      | 15.5-19.5                |            |        | Contrôle des classeurs |
| 12      | 22.5-26.5                |            |        |                        |
|         | Vacances de la Pentecôte |            |        |                        |
| 13      | 5.6-9.6                  | Chapitre 4 |        |                        |
| 14      | 12.6-16.6                |            |        | Contrôle des classeurs |
| 15      | 19.6-23.6 (-2)           |            |        | DEC II,2               |
| 16      | 26.6-30.6                |            |        | Contrôle des classeurs |
| 17      | 3.7-6.7                  |            |        |                        |
|         |                          |            |        |                        |
|         |                          |            |        |                        |
|         |                          |            |        |                        |
|         |                          |            |        |                        |
