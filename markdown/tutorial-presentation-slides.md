


Les présentations (*slides*) sont rapides à produire, faciles à mettre à jour et constituent un moyen efficace de stimuler l'intérêt visuel dans presque toutes les présentations.

## Conseils pour réaliser des présentations PowerPoint efficaces

1. Utilisez un modèle de conception simple et cohérent. Vous pouvez varier la présentation du contenu (liste à puces, texte sur deux colonnes, texte et image, etc.), mais soyez cohérent avec les autres éléments tels que la police, les couleurs et le fond.
2. Simplifiez et limitez le nombre de mots sur chaque écran. Utilisez des phrases clés et n'incluez que les informations essentielles.
3. Limitez la ponctuation et évitez de mettre les mots en majuscules. Les espaces vides sur la diapositive améliorent la lisibilité.
4. Utilisez des couleurs contrastées pour le texte et le fond. Un texte clair sur un fond sombre est préférable. Les arrière-plans à motifs peuvent réduire la lisibilité.
5. Évitez d'utiliser des transitions tape-à-l'œil, telles que des incrustations de texte. Ces fonctions peuvent sembler impressionnantes au premier abord, mais elles sont distrayantes et se démodent rapidement.
6. L'utilisation excessive d'effets spéciaux tels que les animations et les sons peut rendre votre présentation "mignonne" et nuire à votre crédibilité.
7. Utilisez des images de bonne qualité qui renforcent et complètent votre message. Assurez-vous que votre image conserve son impact et sa résolution lorsqu'elle est projetée sur un écran plus grand.
8. Si vous utilisez des " builds " (lignes de texte apparaissant à chaque fois que vous cliquez sur la souris), faites en sorte que le contenu apparaisse à l'écran de manière cohérente et simple ; le mieux est de commencer par le haut ou la gauche. N'utilisez cette fonction que lorsque cela est nécessaire pour faire passer votre message, car les builds peuvent ralentir votre présentation.
9. Limitez le nombre de diapositives. Les présentateurs qui passent constamment à la diapositive suivante risquent de perdre leur public. Une bonne règle de base est une diapositive par minute.
10. Apprenez à naviguer dans votre présentation de manière non linéaire. PowerPoint permet au présentateur d'avancer ou de reculer sans avoir à feuilleter toutes les diapositives intermédiaires.
11. Sachez comment avancer et reculer dans votre présentation et entraînez-vous à le faire. Les spectateurs demandent souvent à revoir un écran précédent.
12. Si possible, visionnez vos diapositives sur l'écran que vous utiliserez pour votre présentation. Assurez-vous que les diapositives sont lisibles depuis les sièges du dernier rang. Le texte et les images graphiques doivent être suffisamment grands pour être lus, mais pas trop au point de paraître "bruyants".
13. Prévoyez un plan B en cas de difficultés techniques. N'oubliez pas que les transparents et les documents à distribuer ne présentent pas d'animation ou d'autres effets spéciaux.
14. Entraînez-vous avec une personne qui n'a jamais vu votre présentation. Demandez-lui un retour honnête sur les couleurs, le contenu et les effets ou images graphiques que vous avez inclus.
15. Ne lisez pas vos diapositives. Le contenu de vos diapositives est destiné au public, pas au présentateur.
16. Ne parlez pas à vos diapositives. De nombreux présentateurs font face à leur présentation sur l'écran plutôt qu'à leur public.
17. Ne vous excusez pas pour quoi que ce soit dans votre présentation. Si vous pensez qu'un élément sera difficile à lire ou à comprendre, ne l'utilisez pas.


## Sources

- [Tips for Making Effective PowerPoint Presentations](https://www.ncsl.org/legislative-staff/lscc/tips-for-making-effective-powerpoint-presentations)
