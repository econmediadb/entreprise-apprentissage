# Evaluation

## 1. Mode d'évaluation

- Epreuve écrite
- Exposé
- Entretien professionnel
- Portfolio
- Evaluation continue des compétences

## 2. Compétences 

1. L’élève est capable d’expliquer les activités de l’entreprise d’apprentissage, les différents services, les tâches des services, ainsi que les conditions générales des ventes. (**12 points**)
  - Les activités, les produits et les services de l’entreprise d'apprentissage sont connus.
  - Les services de l’entreprise d’apprentissage
    - Administration et ressources humaines
    - Achats
    - Stocks
    - Ventes et Marketing 
    - Comptabilité 
    - ...
  - et leurs tâches sont identifiés.
  - Les clients, les fournisseurs, les banques, les conditions générales des ventes (CGV), .... sont identifiés.
2. L’élève est capable, dans le contexte de cas pratiques, de compléter et d’appliquer des schémas de déroulement, tout en utilisant les logiciels de manière appropriée et dans des délais impartis. (**24 points**)
  - Les tâches et les étapes des schémas de déroulement sont comprises.
  - Les tâches sont effectuées selon les schémas de déroulements et ceci dans les délais impartis.
  - Les logiciels sont utilisés pour l’élaboration de fichiers / documents et manipulés de manière professionnelle.
3. L’élève est capable de compléter et de mettre à jour son dossier individuel de manière autonome, sur base d’un plan de
classement et ceci dans les délais impartis. (**12 points**)
  - Le dossier est régulièrement mis à jour.
  - Le plan de classement est utilisé et la structure du dossier (plan de classement) est connue.
4. L’élève est capable d'exécuter les conseils de correction obtenus par l'enseignant et d'effectuer un contrôle avant la remise des documents. (**12 points**) 
  - Les consignes sont comprises et exécutées.
  - Le travail est réalisé.

## 3. Checklist

| Compétences | Points | Socles                                                                                                                                |
|-------------|--------|---------------------------------------------------------------------------------------------------------------------------------------|
| 1           |  4     | Les activités, produits et services sont correctement énumérés et expliqués.                                                          |
|             |  4     | L’attribution des tâches aux services respectifs est correcte.                                                                        |
|             |  4     | Les informations sur les partenaires de l’entreprise sont recherchées et les conditions générales sont correctement appliquées.       |
| 2           |  8     | Les tâches et les étapes sont correctement énumérées et expliquées.                                                                   |
|             |  8     | Les schémas de déroulement sont correctement et méthodiquement utilisés et les documents sont mis à jour dans les délais demandés.    |
|             |  8     | Les logiciels utilisés sont adéquats, leur manipulation est appropriée et des formules sont appliquées pour les tableurs.             |
| 3           |  6     | Le dossier est disponible, complet et actualisé dans les délais demandés.                                                             |
|             |  6     | Le plan de classement est disponible et les documents sont rangés soigneusement et de manière autonome d’après le plan de classement. |
| 4           |  6     | Les consignes de travail sont respectées.                                                                                             |
|             |  6     | Le travail est soigné et vérifié avant la remise.                                                                                     |
