# Overview


## Chapter 1

- Les activités et services d'une entreprise: 
  - les produits et services, 
  - historique, 
  - relations internes et externes, 
  - analyse des finances et présentation « PPP » par les élèves
- Les conditions générales d'une entreprise : analyse et application à des cas concrets
- Mise en place du dossier : 
  - les départements, 
  - les intercalaires, 
  - le plan de classement, 
  - le dossier d’information
- Analyse des documents et formulaires mis à disposition des élèves

20% du cours

## Chapter 2 : L'activité principale de l'entreprise

- Le fonctionnement des schémas de déroulement
- Le calcul du prix de vente sur base d’une demande
d’offres aux fournisseurs
- Tableau de comparaison des offres
- Différents cas concrets sont appliqués :
  1. Demande d’offre par un client, sans rupture de stock
  2. Demande d’offre par un client, avec stock inférieur
au stock critique
  3. Demande d’offre par un client, avec rupture de stock
  4. Factures par fournisseurs divers
  5. Ventes au magasin (caisse)

40% du cours

## Chapter 3 : Le dossier

- Contrôle des documents et formulaires : 
  - échéanciers,
  - fiches clients, 
  - fiches articles,...
- Contrôle du classement du dossier : 
  - dossier complet,
  - classement correct
- Contrôle régulier après chaque cas et/ou contrôle final

20% du cours

## Chapter 4 : Le travail dans la société

- Lettres correctes
- Formulaires corrects
- Calculs corrects

20% du cours


